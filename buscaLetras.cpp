#include <ctime>
#include <sys/time.h>
#include <ostream>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>


int contarLetras(char *archivo){

    // Conteo de caracteres
    char c[2];
    int cnt = 0;
    int stream = open(archivo, O_RDONLY);

    if (stream == -1) {
        std::cout << "Error al abrir archivo" << std::endl;
        return 0;
    }
    // Cada caracter en el archivo será contado
    while (read(stream,c,1)) {
        cnt++;
    }
    close (stream);
    return cnt;
}


int contarPalabras(char *archivo){

    // Conteo de palabras
    char c[2];
    int cnt = 0;
    int stream = open(archivo, O_RDONLY);

    if (stream == -1) {
        std::cout << "Error al abrir archivo" << std::endl;
        return 0;
    }

    // para contar palabras se toma en cuenta espacios como saltos de línea
    while (read(stream,c,1)) {
        if (strchr(c, '\n')){
            cnt++;
        }
        else if (strchr(c, ' ')){
            cnt++;
        }
    }
    close (stream);

    return cnt;
}


int contarLineas(char *archivo){

    // Conteo de lineas
    char c[2];
    int cnt = 0;
    int stream = open(archivo, O_RDONLY);

    if (stream == -1) {
        std::cout << "Error al abrir archivo" << std::endl;
        return 0;
    }

    // Todo salto de linea y termino de archivo contara como linea
    while (read(stream,c,1)) {
        if (strchr(c, '\n')){
            cnt++;
        }
    }
    close(stream);

    return cnt;
}

void buscarLetras (int *total, char *archivo) {

    int caracteres = 0;
    int lineas = 0;
    int palabras = 0;
    
    // Cada búsqueda se realiza por separado para comparar con hebras
    caracteres = contarLetras(archivo);
    lineas = contarLineas(archivo);
    palabras = contarPalabras(archivo);

    // Se imprime conteo por cada archivo
    std::cout << "En el archivo <" << archivo << "> hay:" << std::endl;
    std::cout << "Caracteres: " << caracteres << std::endl;
    std::cout << "Palabras: " << palabras << std::endl;
    std::cout << "Lineas: " << lineas << std::endl << std::endl;

    // Se agregan a arreglo que permite juntar todos los valores
    total[0] = caracteres;
    total[1] = palabras;
    total[2] = lineas;
}

int main(int argc, char *argv[]) {

    // arreglo para total de archivos
    int total[argc-1][3];
    int total2[3] = {0,0,0};

    // Contar tiempo de ejecucion
    timeval inicio, fin;
    gettimeofday(&inicio, NULL);

    // Contar en cada archivo por separado
    for (int i=0; i<argc-1; i++){
        buscarLetras(total[i], argv[i+1]);

        // Sumar cantidades de carateres, palabras y lineas por archivo
        total2[0] += total[i][0];
        total2[1] += total[i][1];
        total2[2] += total[i][2];
    }
    gettimeofday(&fin, NULL);
    double tiempo =  (double) fin.tv_sec - inicio.tv_sec;

    // Imprimir resultados finales 
    std::cout << "Total entre archivos:" << std::endl;
    std::cout << "Caracteres: " << total2[0] << std::endl;
    std::cout << "Palabras: " << total2[1] << std::endl;
    std::cout << "Lineas: " << total2[2] << std::endl;
    std::cout << "Tiempo de ejecucion: ~" << tiempo << " s" << std::endl;

    return 0;
}
