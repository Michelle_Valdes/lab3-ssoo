#include <pthread.h>
#include <sys/time.h>
#include <ostream>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>


// Estructura para retornar mas de una variable de una hebra
struct resultados{
    int caracteres;
    int palabras;
    int lineas;
};


void *contarLetras(void *archivo){

    // Conteo de letras 
    char c[2];
    int cnt = 0;
    // Necesario para retornar un valor
    int* resultado = (int *) malloc(sizeof(int));
    char *abrir = (char *) archivo;
    
    int stream = open(abrir, O_RDONLY);

    if (stream == -1) {
        std::cout << "Error al abrir archivo" << std::endl;
    }
    // Se cuenta cada caracter
    while (read(stream,c,1)) {
        cnt++;
    }
    close (stream);
    
    // Se retorna el valor del conteo
    *resultado = cnt;
    return (void *) resultado;
}


void *contarPalabras(void *archivo){

    // Conteo de palabras
    char c[2];
    int cnt = 0;
    char *abrir = (char *) archivo;
    int stream = open(abrir, O_RDONLY);
    // Espacio para retornar valor
    int *resultado = (int *) malloc(sizeof(int));

    if (stream == -1) {
        std::cout << "Error al abrir archivo" << std::endl;
        pthread_exit(0);
    }
    // Para las palabras se cuentan espacios y saltos de linea
    while (read(stream,c,1)) {
        if (strchr(c, '\n')){
            cnt++;
        }
        else if (strchr(c, ' ')){
            cnt++;
        }
    }
    close (stream);

    // Se retorna resultado
    *resultado = cnt;
    return (void *) resultado;
}


void *contarLineas(void *archivo){
    
    // Conteo de lineas
    char c[2];
    int cnt = 0;
    char *abrir = (char *) archivo;
    int stream = open(abrir, O_RDONLY);

    // Espacio para retornar
    int *resultado = (int *) malloc(sizeof(int));

    if (stream == -1) {
        std::cout << "Error al abrir archivo" << std::endl;
        pthread_exit(0);
    }
    // Se lee cada salto de linea
    while (read(stream,c,1)) {
        if (strchr(c, '\n')){
            cnt++;
        }
    }
    close(stream);
    // Retornar resultados
    *resultado = cnt;
    return (void *) resultado;
}


void *buscarLetras (void *param) {

    // Se crean contadores y estructuras para resultados
    int* conteos[3];
    char *archivo = (char *) param;
    pthread_t contadores[3];
    struct resultados *resultado = (struct resultados *) malloc(sizeof(struct resultados));

    // Se crea una hebra para contar lineas, una para caracteres y una para palabras
    pthread_create(&contadores[0], NULL, contarLetras, archivo);
    pthread_create(&contadores[1], NULL, contarPalabras, archivo);
    pthread_create(&contadores[2], NULL, contarLineas, archivo);

    // Se obtienen los valores de cada una
    for (int i=0; i<3; i++){
        pthread_join(contadores[i], (void **)&conteos[i]);
    }

    // Se imprime el resultado de cada archivo
    std::cout << "En el archivo <" << archivo << "> hay:" << std::endl;
    std::cout << "Caracteres: " <<  *conteos[0] << std::endl;
    std::cout << "Palabras: " << *conteos[1] << std::endl;
    std::cout << "Lineas: " << *conteos[2] << std::endl << std::endl;

    // Se guardan los resultados en la estructura
    resultado->caracteres = *conteos[0];
    resultado->palabras = *conteos[1];
    resultado->lineas = *conteos[2];
    // Se libera espacio de resultados ya usados
    free(conteos[0]);
    free(conteos[1]);
    free(conteos[2]);

    return (void *) resultado;
}


int main(int argc, char *argv[]) {

    // Creación de hebras
    pthread_t threads[argc - 1];
    int total[3] = {0,0,0};
    struct resultados *resultadoFinal[argc-1];
    timeval inicio, fin;
    gettimeofday(&inicio, NULL);

    // Se le asigna a cada hebra un archivo distinto
    for (int i=0; i<argc-1; i++){
        pthread_create(&threads[i], NULL, buscarLetras, argv[i+1]);
    }

    // Se espera que cada hebra termine y se suma su resultado al total
    for (int i=0; i<argc-1; i++){
        pthread_join(threads[i], (void **)&resultadoFinal[i]);
        total[0] += resultadoFinal[i]->caracteres;
        total[1] += resultadoFinal[i]->palabras;
        total[2] += resultadoFinal[i]->lineas;
    }
    // Se liberan los espacios creados para los resultados
    free(resultadoFinal[0]);
    free(resultadoFinal[1]);
    free(resultadoFinal[2]);

    // Se cuenta el tiempo final
    gettimeofday(&fin, NULL);
    double tiempo =  (double) fin.tv_sec - inicio.tv_sec;

    // Se imprimen los resultados finales
    std::cout << "Total entre archivos:" << std::endl;
    std::cout << "Caracteres: " << total[0] << std::endl;
    std::cout << "Palabras: " << total[1] << std::endl;
    std::cout << "Lineas: " << total[2] << std::endl;
    std::cout << "Tiempo de ejecucion: ~" << tiempo << " s" << std::endl;

    return 0;
}
