prefix=/usr/local
CC = g++

# Normal
CFLAGS = -O3 -Wall
LDFLAGS = -lpthread

# Profiling, debug
#CFLAGS = -g -pg -Wall -O3 

SRC = buscaLetras.cpp buscaletras2.cpp
OBJ = buscaLetras.o buscaletras2.o

all: buscaletras1 buscaletras2

buscaletras1: buscaLetras.o
	$(CC) $(CFLAGS) -o $@ buscaLetras.o $(LIBS) $(LDFLAGS)

buscaletras2: buscaletras2.o
	$(CC) $(CFLAGS) -o $@ buscaletras2.o $(LIBS) $(LDFLAGS)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)

